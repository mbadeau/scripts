#!/bin/bash
set -x

RS_PREFIX='curl -L https://gitlab.com/greg/scripts/-/raw/master'
SCRIPT='$1'

$RS_PREFIX/$1.sh | bash