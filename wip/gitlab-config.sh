sed -i '
# gitlab_rails['usage_ping_enabled'] = true
gitlab_rails['usage_ping_enabled'] = false
'

sed -i '
# gitlab_rails['seat_link_enabled'] = true
gitlab_rails['seat_link_enabled'] = false
'

sed -i '
# grafana['enable'] = true
grafana['enable'] = false
'

sed -i "s/#\sprometheus_monitoring[\'enable\']\s=\strue/prometheus_monitoring[\'enable\']\s=\sfalse/g" /etc/gitlab/gitlab.rb