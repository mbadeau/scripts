#!/bin/bash
set -ux

echo "where's the raw CI job log?"
read PATH

## this takes raw job output and tells time details on when each stage occured
cat -A $PATH | sed 's/\^\[\[\0\;\m//g;s/\^M\^\[\[0K/\n/g' | grep section | awk -F : '{print $3; system("date +%r $2")}'
