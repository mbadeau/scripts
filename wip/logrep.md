```
#!/bin/bash
FOLDER=$1
echo -e "Processing $1$LOGPATH \n" >> output.txt
egrep -Hn '\ 404\ |\ 500\ |error|fail|warn|killed|denied' $1/var/log/gitlab/*/* | cut -d '/' -f 6- >> output.txt
```

-i - ignore case
-H - with filename
-n - line number

\ 404\ 

egrep -Hn '\ 404\ |\ 500\ |error|fail|warn|killed|denied'

egrep -Hn '\ 404\ '
egrep -Hn '\ 404\ ' */var/log/gitlab/*/*
egrep -Hn '\ 500\ '
egrep -Hn '\ 500\ ' */var/log/gitlab/*/*

egrep -Hni '\"error\"'
egrep -Hni '\"error\"' */var/log/gitlab/*/*
egrep -Hni '\"fail\"' */var/log/gitlab/*/* 
egrep -Hni '\"failure\" 
grep -Hni 'killed' */var/log/gitlab/*/*

gitaly/current
egrep -Hn 'error|fail|warn' gitlabsos/*/var/log/gitlab/gitaly/current
egrep -Hni 'error|fail|warn' gitlabsos/*/var/log/gitlab/gitlab-rails/application.log 
egrep -Hn 'error|fail|warn' gitlabsos/*/var/log/gitlab/gitlab-rails/production.log
egrep -Hn 'error|fail|warn' gitlabsos/*/var/log/gitlab/gitlab-rails/

```bash
#!/bin/bash
FOLDER=$1
echo -e "Processing $1$LOGPATH \n" >> output.txt
egrep -Hn '\ 404\ |\ 500\ |error|fail|warn|killed|denied' $1/var/log/gitlab/*/* | cut -d '/' -f 6- >> output.txt
```

`grep -Ev "^$|^\s*#|password|keys" gitlab.rb`