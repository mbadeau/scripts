#!/bin/bash
set -ux

USERNAME=root
USER_ID=1
USER_IP=127.0.0.1

grep -Er "username\"\:\"$USERNAME|user_id\"\:$USER_ID|$USER_IP" /var/log/gitlab/gitlab-rails | tee -a $USERNAME-$USER_ID-today.log

grep -E "username\"\:\"$USERNAME|user_id\"\:$USER_ID|$USER_IP" /var/log/gitlab/gitlab-rails/api_json.log | tee -a $USERNAME-$USER_ID-api.log
zgrep -E "username\"\:\"$USERNAME|user_id\"\:$USER_ID|$USER_IP" /var/log/gitlab/gitlab-rails/api_json.log.*.gz | tee -a $USERNAME-$USER_ID-api.log

grep -E "username\"\:\"$USERNAME|user_id\"\:$USER_ID|$USER_IP" /var/log/gitlab/gitlab-rails/production_json.log | tee -a $USERNAME-$USER_ID-production.log
zgrep -E "username\"\:\"$USERNAME|user_id\"\:$USER_ID|$USER_IP" /var/log/gitlab/gitlab-rails/production_json.log | tee -a $USERNAME-$USER_ID-production.log
