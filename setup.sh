#!/bin/bash

chmod +x *.sh
./tzdata.sh
./swapon.sh
./support-utils.sh
./rust-utils
./happy-redis.sh
./dir-colors.sh
./gitlab-prep.sh
./docker.sh
./tldr.sh