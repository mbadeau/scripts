sudo apt install -y apt-utils apt-transport-https autoconf automake bash-completion bmon ca-certificates curl dos2unix ffmpeg fonts-dejavu-core fonts-dejavu-extra fonts-open-sans fonts-roboto fonts-ubuntu fwupd fwupdate git gnupg-agent jq lsof lnav make manpages mc nmap openssl redshift redshift-gtk rename silversearcher-ag software-properties-common sudo time tmux tilix tree unrar unzip vim wget whois xclip zip

curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu eoan stable"

sudo apt-get update

sudo apt-get install google-cloud-sdk kubectl nodejs docker-ce docker-ce-cli containerd.io git-lfs

curl -L https://gitlab.com/greg/scripts/-/raw/master/rust-utils.sh | sudo bash

## GUI

## UI/theme

sudo apt install arc-theme adapta-gtk-theme numix-icon-theme numix-blue-gtk-theme

https://github.com/gpakosz/.tmux