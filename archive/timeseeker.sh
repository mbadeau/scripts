#!/bin/bash
set -ux

# hh:mm:ss
TIME=00:00:00
# YYYY-MM-YY
DATE=2020-04-28

echo "$DATE $TIME"
date -d "$DATE $TIME" +%s
