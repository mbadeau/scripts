#!/bin/bash

## install fd rg and bat on debian/ubuntu

mkdir /tmp/rust
cd /tmp/rust || exit
curl -LO https://github.com/sharkdp/bat/releases/download/v0.17.1/bat_0.17.1_amd64.deb
curl -LO https://github.com/sharkdp/fd/releases/download/v8.2.1/fd_8.2.1_amd64.deb
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/12.1.1/ripgrep_12.1.1_amd64.deb
sudo apt install -y ./*.deb
rm -rf /tmp/rust
cd "$HOME" || exit
