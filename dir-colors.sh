#!/bin/bash

curl -L -o ~/.dir_colors https://raw.githubusercontent.com/arcticicestudio/nord-dircolors/develop/src/dir_colors
test -r "$HOME/.dir_colors" && eval '$(dircolors ~/.dir_colors)'
echo 'test -r "~/.dir_colors" && eval $(dircolors ~/.dir_colors)'
