#!/bin/bash
# set -x

sudo gitlab-ctl stop
sudo gitlab-ctl cleanse &&
systemctl stop gitlab-runsvdir
# ubuntu/debian
sudo apt purge gitlab-ee -y
# centos/rhel
# sudo yum remove gitlab-ee
sudo rm -rf /opt/gitlab/sv
sudo rm -rf /opt/gitlab/init
sudo rm -rf /opt/gitlab/etc
sudo rm -rf /opt/gitlab/embedded/ssl/certs
sudo rm -rf /opt/gitlab/embedded/service/gitlab-shell
sudo rm -rf /opt/gitlab/embedded/service/gitlab-rails/public
sudo rm -rf /opt/gitlab/embedded/service/gitlab-rails/config
sudo rm -rf /opt/gitlab/embedded/service/gitlab-rails/changelogs/unreleased
sudo rm -rf /opt/gitlab/embedded/service/gitlab-rails/app/workers
sudo rm -rf /opt/gitlab/embedded/service/gitlab-rails/app/services/projects
sudo rm -rf /opt/gitlab/embedded/cookbooks
sudo rm -rf /opt/gitlab/embedded/bin
sudo rm -rf /etc/sysctl.d/*gitlab*
echo "GitLab successfully removed."
