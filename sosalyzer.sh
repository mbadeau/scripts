#!/bin/sh
set -x

fast-stats "$1/var/log/gitlab/gitlab-rails/production_json.log" | tee -a stats.txt
fast-stats "$1/var/log/gitlab/gitaly/current" | tee -a stats.txt
fast-stats "$1/var/log/gitlab/sidekiq/current" | tee -a stats.txt
fast-stats "$1/var/log/gitlab/gitlab-rails/api_json.log" | tee -a stats.txt
grep -EHnri "\ 4[0-9][0-9]\ |\ 5[0-1][0-9]\ |error|fail|warn|killed|denied" "$1/var/log/gitlab" | tee -a err.log