#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt update
ln -fs /usr/share/zoneinfo/America/Denver /etc/localtime &&
apt-get install -y tzdata
dpkg-reconfigure --frontend noninteractive tzdata
